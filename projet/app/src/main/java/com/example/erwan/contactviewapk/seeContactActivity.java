package com.example.erwan.contactviewapk;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import org.json.JSONException;

import java.io.IOException;

public class seeContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context context = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_contact);


        try {
            BDManagement db = new BDManagement();
            Log.i("contactGenerer", "Je rentre");
            for (Contact c : db.getContact(context)) {
                LinearLayout layout = findViewById(R.id.allContacts);
                Button button = new Button(this);
                button.setText(c.getNom());
                button.setId(c.getID());
                Log.i("contactGenerer", String.valueOf(c.getID()));
                button.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent in = new Intent(context, ContactManagementActivity.class);
                        in.putExtra("IDCONTACT", String.valueOf(v.getId()));
                        context.startActivity(in);
                        Log.i("contactSelect", String.valueOf(v.getId()));
                    }
                });
                layout.addView(button);

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void retourHome(View view) {
        finish();
    }
}
