package com.example.erwan.contactviewapk;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONException;

import java.io.IOException;
import java.util.Calendar;

public class edit_Contact extends AppCompatActivity {

    private Contact contact;
    private EditText upFName;
    private EditText upname;
    private EditText upPhone;
    private CheckBox upFav;
    private Spinner upType;
    private String dataSelected = "1/1/1990";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__contact);

        contact = (Contact) getIntent().getSerializableExtra("ContactOBJECT");
        Log.i("editCONTACT", "le contact " + contact.getID() + " est ici !");

        upFName = findViewById(R.id.contactfnameUpload);
        upname = findViewById(R.id.contactNameUpload);
        CalendarView upBirthday = findViewById(R.id.birthdayContactUpload);
        upPhone = findViewById(R.id.phoneContactUpload);
        upFav = findViewById(R.id.UploadfavContactCheck);
        upType = findViewById(R.id.UploadtypeContactSpinn);

        upFName.setText(contact.getPrenom());
        upname.setText(contact.getNom());

        String mDate[] = contact.getDateNaissance().split("/");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(mDate[2]));
        calendar.set(Calendar.MONTH, Integer.parseInt(mDate[1]) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(mDate[0]));

        upBirthday.setDate(calendar.getTimeInMillis(), true, true);
        upPhone.setText(contact.getNumTel());
        upFav.setChecked(contact.isFavoris());


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.typeContact, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        upType.setAdapter(adapter);
        if (contact.getType() != null) {
            int pos = adapter.getPosition(contact.getType());
            upType.setSelection(pos);
        }

        upBirthday.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                String date = dayOfMonth + "/" + (month + 1) + "/" + year;
                Log.i("DateChoisis", date);
                dataSelected = date;
                contact.setDateNaissance(dataSelected);
            }
        });
    }


    public void retourCM(View view) {
        finish();
    }

    public void validerModif(View view) {
        final Context context = this;

        if (upname.getText().toString().isEmpty())
        {
            upname.setError("Le nom n'est pas facultatif");
            upname.setFocusable(true);
        }
        else if(upFName.getText().toString().isEmpty())
        {
            upFName.setError("Le prenom n'est pas facultatif");
            upFName.setFocusable(true);
        }
        else {
            contact.setPrenom(upFName.getText().toString());
            contact.setNom(upname.getText().toString());
            contact.setNumTel(upPhone.getText().toString());
            contact.setFavoris(upFav.isChecked());
            contact.setType(upType.getSelectedItem().toString());

            new AlertDialog.Builder(this)
                    .setTitle("Modification de " + contact.getPrenom() + " " + contact.getNom())
                    .setMessage(
                            "Voulez-vous vraiment le modifier ?"
                    )
                    .setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            BDManagement db = new BDManagement();
                            try {
                                db.updateContact(context, contact);
                                startActivity(new Intent(context, MainActivity.class));
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    })
                    .setNegativeButton("NON", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }

    }
}
