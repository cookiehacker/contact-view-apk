package com.example.erwan.contactviewapk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;

public class ContactManagementActivity extends AppCompatActivity {

    private Contact c;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_management);

        BDManagement db = new BDManagement();

        String idcontact = getIntent().getStringExtra("IDCONTACT");

        try {
            c = db.getContactByID(this, Integer.parseInt(idcontact));

            TextView title = findViewById(R.id.contactViewT);
            TextView birhday = findViewById(R.id.contactViewBirthday);
            TextView phone = findViewById(R.id.contactViewPhone);
            TextView fav = findViewById(R.id.contactViewFav);
            TextView type = findViewById(R.id.contactViewType);

            title.setText(c.getNom() + " " + c.getPrenom());
            birhday.setText(c.getDateNaissance());
            phone.setText(c.getNumTel());
            type.setText(c.getType());
            if (c.isFavoris()) fav.setText("Il/Elle est votre favoris");
            else fav.setText("Il/Elle n'est pas votre favoris");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void retourSeeContact(View view) {
        finish();
    }

    public void editContact(View view) {
        if (c != null) {
            Intent intent = new Intent(this, edit_Contact.class);
            intent.putExtra("ContactOBJECT", c);
            this.startActivity(intent);
        }
    }

    public void commContact(View view) {
        if (c != null) {
            Intent intent = new Intent(this, CommunicationActivity.class);
            intent.putExtra("ContactOBJECT", c);
            this.startActivity(intent);
        }
    }

    public void deleteContact(View view) {
        if (c != null) {
            final Context context = this;
            new AlertDialog.Builder(this)
                    .setTitle("Suppression de " + c.getPrenom() + " " + c.getNom())
                    .setMessage(
                            "Êtes vous sûr de vouloir le supprimer ?"
                    )
                    .setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            BDManagement db = new BDManagement();
                            try {
                                db.deleteContact(context, c);
                                startActivity(new Intent(context, MainActivity.class));
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    })
                    .setNegativeButton("NON", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
    }

}
