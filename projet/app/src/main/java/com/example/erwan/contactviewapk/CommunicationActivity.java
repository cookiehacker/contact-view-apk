package com.example.erwan.contactviewapk;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;

public class CommunicationActivity extends AppCompatActivity {

    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);

        contact = (Contact) getIntent().getSerializableExtra("ContactOBJECT");
    }

    public void dialPhoneNumber(View view) {
        try {
            Intent appel = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact.getNumTel()));
            startActivity(appel);
        } catch (SecurityException e) {
            new AlertDialog.Builder(this)
                    .setTitle("Erreur de l'envoie du message à " + contact.getPrenom() + " " + contact.getNom())
                    .setMessage(
                            "Désolé ! Votre telephone ne donne pas la permission pour les appels"
                    )
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }

    }

    public void retourAuxInfo(View view) {
        finish();
    }

    public void sendMessage(View view) {
        EditText mess = findViewById(R.id.smsEdit);
        String message = mess.getText().toString();

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(contact.getNumTel(), null, message, null, null);

            new AlertDialog.Builder(this)
                    .setTitle("Envoie message à " + contact.getPrenom() + " " + contact.getNom())
                    .setMessage(
                            "Message envoyé !"
                    )
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        } catch (SecurityException e) {
            new AlertDialog.Builder(this)
                    .setTitle("Erreur de l'envoie du message à " + contact.getPrenom() + " " + contact.getNom())
                    .setMessage(
                            "Désolé ! Votre telephone ne donne pas la permission pour l'envoie de SMS"
                    )
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }

    }
}
