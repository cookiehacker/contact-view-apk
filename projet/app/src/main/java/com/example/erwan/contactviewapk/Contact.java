package com.example.erwan.contactviewapk;


import java.io.Serializable;

public class Contact implements Serializable {
    private int idContact;
    private String nom;
    private String prenom;
    private String dateNaissance;
    private String numTel;
    private boolean favoris;
    private String type;

    public Contact(int id, String nom, String prenom, String dateNaissance, String numTel, boolean favoris, String type) {
        this.idContact = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.numTel = numTel;
        this.favoris = favoris;
        this.type = type;
    }

    public int getID() {
        return idContact;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public boolean isFavoris() {
        return favoris;
    }

    public void setFavoris(boolean favoris) {
        this.favoris = favoris;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
