package com.example.erwan.contactviewapk;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context context = this;

        File bd = new File(this.getFilesDir() + "/bd_Contact.json");

        if (!bd.exists()) {
            new AlertDialog.Builder(this)
                    .setTitle("Gestion de la BD")
                    .setMessage(
                            "Hey toi ! Tu es nouveau ?\n"
                                    + "T'inquiéte pas ! Ta jolie petite base de données est en création pour que tu puisses y stocker tout tes contacts !" +
                                    " Aller ! clique sur 'OK' pour utiliser cette application :) "
                    )
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            File filebd = new File(context.getFilesDir(), "bd_Contact.json");
                            try {
                                boolean val = filebd.createNewFile();
                                Log.i("testCreationFichier", String.valueOf(val));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).show();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void lancerAjoutContact(View view) {
        Log.i("Changement de page", "Lancement de la page d'ajout de contacts");
        Intent intent = new Intent(this, AddContactActivity.class);
        startActivity(intent);
    }

    public void lancerVoirContact(View view) {
        Intent intent = new Intent(this, seeContactActivity.class);
        startActivity(intent);
    }
}
