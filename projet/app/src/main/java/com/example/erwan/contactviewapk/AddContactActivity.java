package com.example.erwan.contactviewapk;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONException;

import java.io.IOException;
import java.util.Calendar;

public class AddContactActivity extends AppCompatActivity {

    private String dataSelected = "1/1/1990";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 1990);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        CalendarView calendarView = findViewById(R.id.birthdayContactEdit);
        calendarView.setDate(calendar.getTimeInMillis(), true, true);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                String date = dayOfMonth + "/" + (month + 1) + "/" + year;
                Log.i("DateChoisis", date);
                dataSelected = date;
            }
        });

        Spinner spinner = findViewById(R.id.typeContactSpinn);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.typeContact, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

    }

    public void addContact(View view) throws IOException, JSONException {
        final Context context = this;

        // CONNEXION
        BDManagement db = new BDManagement();

        // FORM DUMP
        EditText nomEdit = findViewById(R.id.contactNameEdit);
        EditText fnameEdit = findViewById(R.id.contactfnameEdit);
        EditText phoneEdit = findViewById(R.id.phoneContactEdit);
        CheckBox favEdit = findViewById(R.id.favContactCheck);
        Spinner typeEdit = findViewById(R.id.typeContactSpinn);

        // CONVERT TO CORRECT TYPE
        String nom = nomEdit.getText().toString();
        String prenom = fnameEdit.getText().toString();
        String numTel = phoneEdit.getText().toString();
        Boolean favoris = favEdit.isChecked();
        String type = typeEdit.getSelectedItem().toString();

        if (nom.isEmpty())
        {
            nomEdit.setError("Le nom n'est pas facultatif");
            nomEdit.setFocusable(true);
        }
        else if(prenom.isEmpty())
        {
            fnameEdit.setError("Le prenom n'est pas facultatif");
            fnameEdit.setFocusable(true);
        }
        else {
            // CREATION OF CONTACT OBJECT
            final Contact c = new Contact(
                    db.getNextID(this),
                    nom,
                    prenom,
                    dataSelected,
                    numTel,
                    favoris,
                    type
            );

            new AlertDialog.Builder(this)
                    .setTitle("Ajout de " + c.getPrenom() + " " + c.getNom())
                    .setMessage(
                            "Voulez-vous vraiment l'ajouter ?"
                    )
                    .setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            BDManagement db = new BDManagement();
                            try {
                                db.insertContact(context, c);
                                finish();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    })
                    .setNegativeButton("NON", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }

    }

    public void retour(View view) {
        finish();
    }

}
