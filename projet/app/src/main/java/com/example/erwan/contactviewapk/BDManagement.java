package com.example.erwan.contactviewapk;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class BDManagement {

    public void insertContact(Context context, Contact c) throws IOException, JSONException {
        JSONObject json = new JSONObject();
        json.put("id", getNextID(context));
        json.put("nom", c.getNom());
        json.put("prenom", c.getPrenom());
        json.put("dateNaissance", c.getDateNaissance());
        json.put("numTel", c.getNumTel());
        json.put("favoris", c.isFavoris());
        json.put("type", c.getType());
        Log.i("testINSERT", c.getDateNaissance());
        try {
            Writer output;
            File file = new File(context.getFilesDir(), "bd_Contact.json");
            output = new BufferedWriter(new FileWriter(file, true));
            output.write(json.toString() + "\n");
            output.close();
            Log.i("testINSERT", json.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Contact> getContact(Context context) throws IOException, JSONException {
        List<Contact> res = new ArrayList<>();
        FileReader read = new FileReader(context.getFilesDir() + "/bd_Contact.json");
        BufferedReader br = new BufferedReader(read);
        String line;

        while ((line = br.readLine()) != null) {
            JSONObject json = new JSONObject(line);

            Contact c = new Contact(
                    json.getInt("id"),
                    json.getString("nom"),
                    json.getString("prenom"),
                    json.getString("dateNaissance"),
                    json.getString("numTel"),
                    json.getBoolean("favoris"),
                    json.getString("type"));

            Log.i("testIDgetContact", c.getNom());
            res.add(c);

        }
        return res;
    }

    public void updateContact(Context context, Contact c) throws IOException, JSONException {
        List<Contact> lsContact = getContact(context);

        int indice = 0;

        for (Contact elem : lsContact) {
            if (elem.getID() == c.getID()) {
                Log.i("testUPDATE", "Je passe ici !");
                lsContact.set(indice, c);
                break;
            }
            indice += 1;
        }

        File file = new File(context.getFilesDir(), "bd_Contact.json");
        file.delete();
        file.createNewFile();

        for (Contact elem : lsContact) {
            insertContact(context, elem);
        }
    }

    public void deleteContact(Context context, Contact c) throws IOException, JSONException {
        List<Contact> lsContact = getContact(context);

        int indice = 0;

        for (Contact elem : lsContact) {
            if (elem.getID() == c.getID()) {
                lsContact.remove(indice);
                break;
            }
            indice++;
        }

        File file = new File(context.getFilesDir(), "bd_Contact.json");
        file.delete();
        file.createNewFile();

        for (Contact elem : lsContact) {
            insertContact(context, elem);
        }
    }

    public Contact getContactByID(Context context, int idContact) throws IOException, JSONException {
        Contact res = null;
        FileReader read = new FileReader(context.getFilesDir() + "/bd_Contact.json");
        BufferedReader br = new BufferedReader(read);
        String line;

        while ((line = br.readLine()) != null) {
            JSONObject json = new JSONObject(line);

            if (json.getInt("id") == idContact) {
                res = (new Contact(
                        json.getInt("id"),
                        json.getString("nom"),
                        json.getString("prenom"),
                        json.getString("dateNaissance"),
                        json.getString("numTel"),
                        json.getBoolean("favoris"),
                        json.getString("type")
                ));
                break;
            }
        }
        return res;
    }

    public int getNextID(Context context) throws IOException, JSONException {
        FileReader file = new FileReader(context.getFilesDir() + "/bd_Contact.json");
        BufferedReader br = new BufferedReader(file);
        String s;
        int res = 0;
        while ((s = br.readLine()) != null) {
            JSONObject l = new JSONObject(s);
            if (l.getInt("id") > res) res = l.getInt("id");
        }
        return res + 1;
    }
}
